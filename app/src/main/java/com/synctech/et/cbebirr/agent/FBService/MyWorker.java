package com.synctech.et.cbebirr.agent.FBService;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;


public class MyWorker extends Worker {


    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        SimulateNotification(getApplicationContext());

//        new Thread() {
//            public void run() {
//                (new Runnable() {
//                    public void run() {
//                        Intent i = new Intent (getApplicationContext(), DDBMain.class);
//                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(i);
//                    }
//                });
//            }
//        }.start();

        return Result.success();
    }


    /* TODO
    *  first run test worker
    *  when data send prepare to parse
    *  proccess the data
    *  send the return
    *  consider on scaling
    * */


    public void SimulateNotification(Context context){
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
//                .setContentTitle("title")
//                .setContentText("message")
//                .setStyle(new NotificationCompat.BigTextStyle().bigText("message"))
//                .setTicker("tickerText")
//                .setColor(Color.BLACK)
//                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher_round))
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri);
////                .addAction(new NotificationCompat.Action(R.mipmap.ic_launcher_round,"Call to " + number,pendingIntentForCall));
//
//
//        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, notificationBuilder.build());
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Uri.encode("*999#")));
//            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Uri.encode("*"+USSDNUM+"*"+msg+"#")));
//            getstartActivity(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);




    }






}
