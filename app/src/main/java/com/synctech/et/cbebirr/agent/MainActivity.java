package com.synctech.et.cbebirr.agent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.synctech.et.cbebirr.agent.FBService.MyWorker;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public OneTimeWorkRequest workRequest;
    public static LifecycleOwner bb;
    public static TextView txv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bb = this;

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
//                        String msg = getString(R.string.msg_token_fmt, token);
//                        Log.d(TAG, msg);
                        Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();

                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("FCM ID", token);
                        clipboard.setPrimaryClip(clip);

                    }
                });



        Bundle ms = getIntent().getExtras();


        TextView txt = findViewById(R.id.text);

        Uri ll = Uri.parse("*847*3*1*Samson*Asefa*Alemu*Tigist#");
        txt.setText(ll.toString());
//
        if(ms != null){

            String lms = ms.getString("msg","not set!");
            txt.setText(lms);
        }


//          workRequest = new OneTimeWorkRequest.Builder(MyWorker.class).build();
         txv = findViewById(R.id.textView);



        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                 OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(MyWorker.class).build();

                workRequest = new OneTimeWorkRequest.Builder(MyWorker.class).build();

                WorkManager.getInstance(getApplicationContext()).enqueue(workRequest);

                WorkManager.getInstance(getApplicationContext()).getWorkInfoByIdLiveData(workRequest.getId())
                        .observe(bb, new Observer<WorkInfo>() {
                            @Override
                            public void onChanged(WorkInfo workInfo) {
                                if(workInfo != null) {
                                    String status = workInfo.getState().name();
                                    txv.append(status + "\n ");
                                }
                            }
                        });
            }
        });





    }


    public static void SetTextView(String tvtxt){
//        TextView txv = findViewById(R.id.textView);
        txv.append(tvtxt);
    }
}
