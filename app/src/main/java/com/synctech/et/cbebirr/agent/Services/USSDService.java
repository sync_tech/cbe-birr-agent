package com.synctech.et.cbebirr.agent.Services;


import android.accessibilityservice.AccessibilityButtonController;
import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.synctech.et.cbebirr.agent.FBService.MyWorker;
import com.synctech.et.cbebirr.agent.FBService.USSDHandleWorker;
import com.synctech.et.cbebirr.agent.MainActivity;
import com.synctech.et.cbebirr.agent.MainActivity.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.synctech.et.cbebirr.agent.FBService.MyFirebaseMessagingService.workRequest;


public class USSDService extends AccessibilityService {

    private AccessibilityNodeInfo source;
    private LifecycleOwner lco;
    String firstName = "";

    public static String TAG = USSDService.class.getSimpleName();

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.d(TAG, "onAccessibilityEvent");


        source = event.getSource();

        /* if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !event.getClassName().equals("android.app.AlertDialog")) { // android.app.AlertDialog is the standard but not for all phones  */
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !String.valueOf(event.getClassName()).contains("AlertDialog")) {
//            Toast.makeText(getApplicationContext() , "AlertDialog" ,Toast.LENGTH_SHORT).show();
            return;
        }
        if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && (source == null || !source.getClassName().equals("android.widget.TextView"))) {
//            Toast.makeText(getApplicationContext() , "android.widget.TextView" ,Toast.LENGTH_SHORT).show();
            return;

        }
        if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && TextUtils.isEmpty(source.getText())) {
//            Toast.makeText(getApplicationContext() , "Empty" ,Toast.LENGTH_SHORT).show();
            return;
        }


        AccessibilityNodeInfo rootNode = getRootInActiveWindow();
        refreshChildViews(rootNode);


        List<CharSequence> eventText;


        if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            eventText = event.getText();
        } else {
            eventText = Collections.singletonList(source.getText());
        }

        String text = processUSSDText(eventText);


        if( TextUtils.isEmpty(text) ) return;

        // Close dialog
        performGlobalAction(GLOBAL_ACTION_BACK); // This works on 4.1+ only

        Log.d(TAG, text);
        // Handle USSD response here
        Toast.makeText(getApplicationContext(),text,Toast.LENGTH_SHORT).show();



        workRequest = new OneTimeWorkRequest.Builder(USSDHandleWorker.class).build();

        WorkManager.getInstance(getApplicationContext()).enqueue(workRequest);

        WorkManager.getInstance(getApplicationContext()).getWorkInfoByIdLiveData(workRequest.getId())
                .observe( MainActivity.bb , new Observer<WorkInfo>() {
                    @Override
                    public void onChanged(WorkInfo workInfo) {
                        if(workInfo != null) {
                            if(workInfo.getState().isFinished()){
                                Data data = workInfo.getOutputData();

                                firstName = data.getString("Enter First Name");

//                                MainActivity.SetTextView(status + "\n ");
                                Toast.makeText(getApplicationContext(), "firstName " + firstName,Toast.LENGTH_LONG).show();

                            }

                            String status = workInfo.getState().name();
                            MainActivity.SetTextView(status + "\n ");
                            Toast.makeText(getApplicationContext(), "Status " + status,Toast.LENGTH_LONG).show();
                        }
                    }
                });


    }



//
    private void refreshChildViews(AccessibilityNodeInfo rootNode){
        int childCount = rootNode.getChildCount();
        for(int i=0; i<childCount ; i++){

            AccessibilityNodeInfo tmpNode = rootNode.getChild(i);
            CharSequence text = tmpNode.getText();

            int subChildCount = tmpNode.getChildCount();

            if(subChildCount==0){
                if(tmpNode.getClassName().toString().contentEquals("android.widget.EditText")){

                    if(text != null) {
//                        Log.d(TAG+"!!!!!!!!", text.toString());
                        tmpNode.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, SetUSSDText(text.toString()));
                    }
//                    clickSend();

                }

                return;
            }

            if(subChildCount>0){
//                Toast.makeText(getApplicationContext(),"subChildCount initial"+subChildCount,Toast.LENGTH_SHORT).show();

                refreshChildViews(tmpNode);
            }

        }
    }

    private String processUSSDText(List<CharSequence> eventText) {
        for (CharSequence s : eventText) {
            String text = String.valueOf(s);
            // Return text if text is the expected ussd response
            if( true ) {
                return text;
            }
        }
        return null;
    }




    @Override
    public void onInterrupt() {
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d(TAG, "onServiceConnected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.flags = AccessibilityServiceInfo.DEFAULT;
        info.packageNames = new String[]{"com.android.phone"};
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED | AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        info.getSettingsActivityName();
        setServiceInfo(info);
//        Log.d("infoname",info.getSettingsActivityName());
    }



    private void clickCancle(){
        if (source != null) {
            for (int i = 0; i < source.getChildCount(); i++) {
                AccessibilityNodeInfo child = source.getChild(i);

                if (child != null) {
                    CharSequence text = child.getText();
                    if (text != null && child.getClassName().equals(Button.class.getName())) {
                        if((text.toString().toLowerCase().equals("cancel"))) {
                            child.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        }
                    }
                }
            }
        }
    }



    private void clickSend() {

        if (source != null) {
            for (int i = 0; i < source.getChildCount(); i++) {
                AccessibilityNodeInfo child = source.getChild(i);

                if (child != null) {
                    CharSequence text = child.getText();
                    if (text != null && child.getClassName().equals(Button.class.getName())) {
                        if ((text.toString().toLowerCase().equals("send"))) {
                            child.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            clickOK();
                        }
                    }
                }
            }
        }
    }



    private void clickOK() {

        if (source != null) {
            for (int i = 0; i < source.getChildCount(); i++) {
                AccessibilityNodeInfo child = source.getChild(i);
                if (child != null) {

                    CharSequence text = child.getText();
                    if (text != null && child.getClassName().equals(Button.class.getName())) {

                        if ((text.toString().toLowerCase().equals("ok"))) {
                            child.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        }
                    }

                }
            }

        }
    }


    private Bundle SetUSSDText(String ussdRequest){

        Bundle arguments = new Bundle();

        if(ussdRequest.startsWith("Enter First Name")){
            arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, firstName);
        }else{
            arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "else");

        }


        return arguments;

    }

}
