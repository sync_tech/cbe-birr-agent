package com.synctech.et.cbebirr.agent.FBService;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.synctech.et.cbebirr.agent.Models.User;
import com.synctech.et.cbebirr.agent.R;

import org.json.JSONObject;

public class USSDHandleWorker extends Worker {


    public USSDHandleWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {


        Data data = getInputData();

        String hostNum = data.getString("hostNum");
        String firstName = data.getString("Enter First Name:");
        String fatherName = data.getString("Enter Father Name:");
        String grandFatherName = data.getString("Enter Grandfather Name:");
        String motherName = data.getString("Enter Mother's Name:");
        String DOB = data.getString("Enter Date of Birth (DDMMYYYY):");
        String idCode = data.getString("Enter ID Number:");
        String issuedBy = data.getString("Enter Issued by:");
        String expireDate = data.getString("Enter ID  Expiry Date(DDMMYYYY) (G.C):");
        String phoneNo = data.getString("Enter Customer Phone Number:");
        String OID = data.getString("Enter Operator ID:");
        String PIN = data.getString("Enter PIN:");

        RequestUSSD(getApplicationContext());

        Data toBeRender = new Data.Builder()
                .putString("hostNum:", hostNum)
                .putString("Enter First Name:", firstName)
                .putString("Enter Father Name:", fatherName)
                .putString("Enter Grandfather Name:", grandFatherName)
                .putString("Enter Mother's Name:", motherName)
                .putString("Enter Date of Birth (DDMMYYYY):", DOB)
                .putString("Enter ID Number:", idCode)
                .putString("Enter Issued by:", issuedBy)
                .putString("Enter ID  Expiry Date(DDMMYYYY) (G.C):", expireDate)
                .putString("Enter Customer Phone Number:", phoneNo)
                .putString("Enter Operator ID:", OID)
                .putString("Enter PIN:", PIN)
                .build();

        return Result.success(toBeRender);
    }



    /* TODO
    *  first run test worker
    *  when data send prepare to parse
    *  proccess the data
    *  send the return
    *  consider on scaling
    * */




//    fatherName	"Tsegaye"
//    firstName	"Biruk"
//    gender	"Male"
//    grandFatherName	"Hdjdj"
//    id	12
//    idCode	"I Sksjsk"
//    idType	"Kebele ID"
//    issuedBy	"Jsksj"
//    motherName	"Bdjdhd"
//    phoneNo	"0910008740"
//    status	"Executed"
//    testussd	"*847*3*1*Biruk*Tsegaye*Hdjdj*Bdjdhd#"
//    token	"cIOzMp8E3w4:APA91bFp6tVZD6ZYIWlsxhepCA8_4jzU8eMOQWXWl9OYEbSPE_GSO6z-4zuteRwAvKChx6MEYkfPemS_XwBcDlT7mEe9vIOvfwLx5_PNboWKY9WEe6gY3XRywEIyIagup5s0K1VvyT1v"
//    ussd	"*847*3*1*Biruk*Tsegaye*Hdjdj*Bdjdhd*04121995*1*I Sksjsk*Jsksj*28-11-2019*1*0910008740*124003*5733*1#"
//
    public void RequestUSSD(Context context){

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Uri.encode("*847*3*1#")));
;
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);


    }






}
