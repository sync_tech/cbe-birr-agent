package com.synctech.et.cbebirr.agent.Models;
import com.orm.SugarRecord;

public class User extends SugarRecord {

    String firstName, fatherName, grandFatherName, motherName;
    String DOB;
    String idType;
    String idCode, issuedBy;
    String expireDate;
    String gender;
    String phoneNo;
    String token;

    public User(String firstName, String fatherName, String grandFatherName, String motherName, String DOB,
                String idType, String idCode, String issuedBy, String expireDate, String gender, String phoneNo, String token) {

        this.firstName = firstName;
        this.fatherName = fatherName;
        this.grandFatherName = grandFatherName;
        this.motherName = motherName;
        this.DOB = DOB;
        this.idType = idType;
        this.idCode = idCode;
        this.issuedBy = issuedBy;
        this.expireDate = expireDate;
        this.gender = gender;
        this.phoneNo = phoneNo;
        this.token = token;

    }

    public User() {

    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getGrandFatherName() {
        return grandFatherName;
    }

    public void setGrandFatherName(String grandFatherName) {
        this.grandFatherName = grandFatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String Identification) {
        this.idCode = Identification;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
